// Utility functions :

import { ChatInputCommandInteraction, Interaction, Message } from "discord.js";
import { logErr } from "./glog";

// Try and catch errors from reply
export async function tryReply(interaction : Interaction | Message, message : string) {
    // Catch Fatal error
    try {
        if (interaction instanceof ChatInputCommandInteraction) {
            // Interaction reply
            if (interaction.isRepliable()) {
                try {
                    await interaction.reply(message)
                } catch (error) {
                    logErr(interaction, `Error occured while replying to ChatInputCommandInteraction : ${error}`)
                }
            } else {
                logErr(interaction, "Interaction is not repliable !")
            }
        } else if (interaction instanceof Message) {
            // Message reply
            try {
                await interaction.reply(message)
            } catch(error) {
                logErr(interaction, `Error occured while replying to Message : ${error}`)
            }
        } else {
            // Wrong interaction
            logErr(interaction, "Can't reply : Wrong interaction type !")
        }
    } catch (error) {
        logErr(interaction, `Fatal error replying : ${error}`)
    }
}