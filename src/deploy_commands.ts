// This script is used to deploy slash commands to discord
import { SlashCommandBuilder } from '@discordjs/builders';
import { REST } from '@discordjs/rest';
import { Routes } from 'discord-api-types/v9';
import { config } from './config';
import { PermissionFlagsBits } from 'discord.js';
import * as lang from './lang/fr.json' // Adapt language for declaring commands

/*
    COMMANDS :
        /anniv <string> 22/23 (date object ??)
        /hbhelp 
        /hbinfo
        /hb set <subcommand>
            channel <#channel>
*/

const commands = [
    // HBINFO
	new SlashCommandBuilder()
        .setName('hbinfo')
        .setDescription(lang.commands.info.HELP),

    // HBHELP
	new SlashCommandBuilder()
        .setName('hbhelp')
        .setDescription(lang.commands.help.HELP),

    // ANNIV
	new SlashCommandBuilder()
        .setName('anniv')
        .setDescription(lang.commands.anniv.HELP)
        .addStringOption(option => option.setName("date").setDescription(lang.commands.anniv.SLASH_DATE_DESC)),
    
    // HBSET
    new SlashCommandBuilder()
        .setName("hbset")
        .setDefaultMemberPermissions(PermissionFlagsBits.ManageChannels) // <-- This is a default and might change
        .setDescription(lang.commands.hbset.DESCRIPTION)
        .addSubcommand(subcommand => subcommand
            .setName("channel")
            .setDescription(lang.commands.hbset.channel.HELP)
            .addChannelOption(option => option
                .setName("channel")
                .setDescription(lang.commands.hbset.channel.ARG_DESC)
            )
        )
]
	.map(command => command.toJSON());

const rest = new REST({ version: '9' }).setToken(config.DISCORD_TOKEN);

rest.put(Routes.applicationCommands(config.BOT_CLIENT_ID), { body: commands })
	.then(() => console.log(lang.misc.commands_correct_register))
	.catch(console.error);
