import dotenv from "dotenv"
import { logErr } from "./glog"

// Get config
dotenv.config()
const { DISCORD_TOKEN , BOT_CLIENT_ID } = process.env

// Log error
if(!DISCORD_TOKEN || !BOT_CLIENT_ID) {
    logErr(undefined, "Missing '.env' file!")
    throw new Error("Missing environment variables! Please set-up your discord token and client ID of your bot into .env file.")
}

// Export Discord Token
export const  config: Record<string, string> = { DISCORD_TOKEN , BOT_CLIENT_ID}