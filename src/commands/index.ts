import { ChatInputCommandInteraction, Interaction, Message } from "discord.js" 
import { log, logErr } from "../glog"
import { Command, Reply } from "./command"
import { VERSION, database, lang } from "../bot"
import { tryReply } from "../utils"

// Init empty command
export var commands : Command[] = []

// Information command
commands.push( new Command(
    "hbinfo",
    "info",
    lang.commands.info.HELP,
    ( _args : string[] , _senderId : string) : Reply => `## HappyBotDay version *${VERSION}*\n> Made by **megaloadon**.\n> git : ` + "``https://gitlab.com/m3galoadon/happy-bot-day``"
))

// Help command
commands.push( new Command(
    "hbhelp",
    "help",
    lang.commands.help.HELP,
    ( _args : string[] , _senderId : string) : Reply => {
        // Init help content
        let rep = lang.commands.help.CONTENT
        // Add codeblock
        rep += "```"
        // Add command helps
        for (const command of commands) {
            rep += `/${command.name} | !hb ${command.oldAlias} : ${command.help}\n`
        }
        // Close codeblock
        rep += "```"
        // Return
        return rep
    }
))

// Set anniv
commands.push(new Command(
    "anniv","anniv",lang.commands.anniv.HELP,
    ( args : string[] , senderId : string) : Reply => {
        // Check args correctly passed
        let arg0Split = args[0].split("/")
        if (arg0Split.length < 2) {
            logErr(undefined, `Malformed command identified ! Args : "${args}" | ID : "${senderId}"`) // TODO More infos
            return `${lang.errors.GENERIC} ${lang.commands.anniv.USAGE}`
        }

        try {
            // Init date object
            let date : [number, number]= [ +arg0Split[0], +arg0Split[1] ]

            // Check for too long entry // Check for NaN entry
            if (date[0] > 31 || date[0] < 0 || date[1] > 12 || date[1] < 0 || isNaN(date[0]) || isNaN(date[1])) {
                logErr(undefined, `Malicious payload into anniv ! Args : "${args}" | ID : "${senderId}"`)
                return `${lang.errors.GENERIC} ${lang.commands.anniv.USAGE}`
            }

            // Add entry
            database.addEntry(senderId, date)
    
            // Reply
            return `${lang.commands.anniv.SUCCESS} : ${date[0]}/${date[1]}`
        } catch(error) {
            // Log error
            logErr(undefined, `Erreur lors de l'ajout d'une date : "${error}" | Args : "${args}" | ID : "${senderId}"`)
            return `${lang.errors.GENERIC} ${lang.commands.anniv.USAGE}`
        }
    }
))

// Execute the command (+log)  
export async function executeCommand(input : Message | Interaction) {
    // Log
    log(input)

    // execute message command
    if (input instanceof Message) {
        // Check if command exist
        let msgSplit = input.content.split(" ")
        for (const command of commands) {
            if (msgSplit[1] == command.oldAlias) {
                // Gather remaining args as string[]
                let args : string[] = []
                for (let i = 2; i < msgSplit.length; i++) {
                    args.push( msgSplit[i] )
                }
                // Execute the function
                await tryReply(input, command.execution(args, input.author.id))
                return
            }
        }
        // Command don't exist
        logErr(input, "Someone issued an unexisting message command!")
        await tryReply(input, lang.commands.errors.UNKNOWN_COMMAND_MSG)
    } else if (input instanceof ChatInputCommandInteraction) {
        // For all commands
        // let interaction = <Interaction> input 
        if (input.isCommand()) {
            for (const command of commands) {
                // Check if the command exist
                if (input.commandName == command.name) {
                    // Change args into strings
                    let args : string[] =  []
                    for (const arg of input.options.data) {
                        args.push( <string> arg.value )
                    }
                    // Execute the function
                    await tryReply(input, command.execution(args, input.user.id))
                    return
                }
            }
            // Command doesn't exist
            logErr(input, "Someone issued an unexisting interraction command!")
            await tryReply(input, lang.commands.errors.UNKNOWN_COMMAND_INT)
        } else {
            // Log error if interaction is not a command
            logErr(input, "Interaction is not a command")
        }
    } else {
        // Input is not Message or Interaction (command)
        logErr(input, "Error executing command : Input is wrong type !")
    }
}