// Reply type alias
export type Reply = string

// Execution function type alias
type ExecutionFunction = (args : string[], senderId : string) => Reply

// Command class
export class Command {
    // Command Name
    public name : string
    // Old command formating alias
    public oldAlias : string
    // Help menu 
    public help : string 
    // Execute Function (taking args)
    public execution : ExecutionFunction

    // Construct Command
    constructor(name : string, oldAlias : string, help : string, executionFunction : ExecutionFunction ) {
        this.execution = executionFunction
        this.help = help
        this.name = name 
        this.oldAlias = oldAlias
    }
}