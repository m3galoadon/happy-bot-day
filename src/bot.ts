// Lang populate
import * as language from "./lang/fr.json"
export const lang = language;

import { ChatInputCommandInteraction, Client, Interaction, Message, PermissionFlagsBits } from "discord.js"  // Discord
import { config } from "./config"                                                       // Config file
import { log, logErr } from "./glog";                                                   // Logging
import { executeCommand } from "./commands";                                            // Command handling
import { checkLoop } from "./checker";                                                  // Check loop
import { Database } from "./database";                                                  // Database
import { hbset } from "./set";                                                          // Admin setting(s)
import { tryReply } from "./utils";                                                     // Utils

// Version 
export const VERSION = "3.1.1"
// Create client
export const client = new Client({ intents: ["Guilds", "GuildMessages", "MessageContent"] });
// Load database
export var database = new Database()

// Once the client is ready
client.once("ready", async () => {
    // Log start prompt
    log( undefined ,"HappyBotDay is up and running" )

    // Check for birthdays at midnight
    checkLoop(0,0)
})

// handle old style commands
client.on("messageCreate", async msg => {
    try {
        // Split message
        let msgSplit = msg.content.split(" ")
    
        // Get prefix
        if (msgSplit[0] == "!hb") {
            // Check for length
            if (msgSplit.length < 2) {
                logErr(msg, "Too short command!")
                return
            } else {
                // Check if member
                if (msg.member) {
                    // Check if the member has permission for set command
                    if (msg.member.permissions.has(PermissionFlagsBits.ManageChannels) && msgSplit[1] == "set") {
                        try {
                            // Apply setting
                            await hbset(msg)
                        } catch(error) {
                            logErr(msg, `An error occured : ${error}`)
                            await tryReply(msg, lang.errors.GENERIC)
                        }
                        return
                    }
                }
                // Execute command
                executeCommand(<Message>msg)
            }
        }
    } catch (error) {
        // Big catch 
        logErr(msg, `Error occured in message reception : ${error}`)
        await tryReply(msg, lang.errors.GENERIC)
    }
})

// handle interaction
client.on("interactionCreate", async interaction => {
    try {
        // Check if /command
        if (interaction.isCommand()) {
            // Check for admin setting command
            if (interaction.commandName == "hbset") {
                // Handle settings
                if (interaction.memberPermissions) {
                    // Check if the member has permission for set command
                    if (interaction.memberPermissions.has(PermissionFlagsBits.ManageChannels)) {
                        try {
                            // Apply settings
                            await hbset(<ChatInputCommandInteraction>interaction)
                        } catch(error) {
                            logErr(interaction, "An error occured while running administrator command!")
                            await tryReply(interaction, lang.errors.GENERIC)
                        }
                        return
                    }
                }
            }
            // Execute command
            executeCommand(<Interaction>interaction)
        }
    } catch (error) {
        // Big catch
        logErr(interaction, `Error occured in Interaction reception : ${error}`)
        await tryReply(interaction, lang.errors.GENERIC)
    }
})

// Log the bot in Discord
client.login(config.DISCORD_TOKEN)