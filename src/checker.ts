// Checks for a birthday into db

import { Date } from "./database";
import { client, database , lang } from "./bot";
import { log, logErr } from "./glog";

// Send a message to everyone into the birthday channels that has this date as birthday
async function check(today : Date) {
    // Init
    log(undefined, `Checking for birthdays on ${today[0]}/${today[1]}`)
    let internal_error = "None"
    try {
        // Iterate through guilds
        for (const guildSetting of database.guildSettings) {
            // get guild
            internal_error = `Resolving guild '${guildSetting.guildId}'`
            try {
                let guild = client.guilds.resolve(guildSetting.guildId)
                if (guild) {
                    // get channel
                    internal_error = `Fetching channel '${guildSetting.birthdayChannelId}'`
                    let channel = await guild.channels.fetch(guildSetting.birthdayChannelId)
                    if (channel) {
                        // Check if text channel
                        if (channel.isTextBased()) {
                            // iterate through birthdays
                            for (const birthday of database.birthdays) {
                                if (birthday.date[0] == today[0] && birthday.date[1] == today[1]) {
                                    // It's their birthday !
                                    // Find if it is into the guild
                                    internal_error = `Fetching member '${birthday.clientId}' from '${guild.id}' (${guild.name})`
                                    try {
                                        let m = await guild.members.fetch(birthday.clientId)
                                        if (m) {
                                            internal_error = `Sending message. MESSAGE : '${lang.checker.success.HAPPY_BIRTHDAY_P1}<@${m.id}>${lang.checker.success.HAPPY_BIRTHDAY_P2}'`
                                            channel.send(`${lang.checker.success.HAPPY_BIRTHDAY_P1}<@${m.id}>${lang.checker.success.HAPPY_BIRTHDAY_P2}`) // TODO : trySend
                                            log(undefined, `Sended happy birthday to ${m.user.username} (${m.id})`)
                                            internal_error = "None"
                                        }
                                    } catch (error) {
                                        // Nothing to log here, it will check for all guilds and all users are not in them
                                        // logErr(undefined, `An error occured while checking birthdays : ${error} ; Internal Error : ${internal_error}`)
                                    }
                                }
                            }
                        }
                    } else {
                        // Channel not found
                        logErr(undefined, `Channel <#${guildSetting.birthdayChannelId}> not found on Guild <${guildSetting.guildId}>!`)
                    }
                } else {
                    // Guild not found
                    logErr(undefined, `Guild <${guildSetting.guildId}> not found!`)
                }
            } catch (error) {
                // Not any more on this guild
                // TODO : remove guild from database
                logErr(undefined, `An error occured while checking birthdays : ${error} ; Internal Error : ${internal_error}`)
            }
        }
        log(undefined, "End of check loop")
    } catch (error) {
        logErr(undefined, `An error occured while checking birthdays : ${error} ; Internal Error : ${internal_error}`)
    }
}

// Sleep function
function sleep(ms: number) {
    return new Promise<void>(function(resolve) {
       setTimeout(resolve, ms);
    })
}

// Loop checking process
export async function checkLoop(executionHour : number, executionMinute : number) {
    // Loop
    while(true) {
        // Get date
        let date = new Date()

        // Check date
        if (executionHour == date.getHours() && executionMinute == date.getMinutes()) {
            log(undefined, "Loop time reached.")
            await check([date.getDate(), date.getMonth() +1])
        }

        // Wait 1 minute
        await sleep(60_000)
    }
}