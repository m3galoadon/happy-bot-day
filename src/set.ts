import { Channel, ChannelType, ChatInputCommandInteraction, Guild, Message } from "discord.js";
import { database, lang } from "./bot";
import { log, logErr } from "./glog";
import { tryReply } from "./utils";

// Parse setting command from Interaction / Message
function parseSettingCommand(object : ChatInputCommandInteraction | Message) : [string, string] {
    // Log
    log(object)

    // Parse arguments and execute set
    let setting : string = ""
    let value   : string = ""

    if (object instanceof ChatInputCommandInteraction) {
        // Parse from interaction
        // Setting name
        let subcommand = (<ChatInputCommandInteraction>object).options.data[0].name
        if (subcommand) {
            if (subcommand != "channel") {
                throw new Error(`Invalid setting : ${subcommand}`)
            } else {
                setting = subcommand 
            }
        } else throw new Error("Setting value not found")
        
        // Value
        let subcommand_argument = (<ChatInputCommandInteraction>object).options.data[0].options 
        if (subcommand_argument) {
            if (subcommand_argument[0].value) {
                value = <string>subcommand_argument[0].value 
            } else throw new Error(`Argument has no value : ${subcommand_argument}`)
        } else throw new Error("No argument provided !")
    } else if (object instanceof Message) {
        // Parse from message
        let msgSplit = object.content.split(" ")
        if (msgSplit.length < 4) throw new Error(`Malformed set command : ${object.content}`)
        if (msgSplit[2] != "channel") throw new Error(`Unrecognised setting : ${msgSplit[2]}`)
        setting = msgSplit[2]
        value = msgSplit[3].replace("<", "").replace(">","").replace("#", "") // F***ing stupid, thanks discord, your own format is not resolvable
    } else throw new Error(`Wrong object type from Instance/Message setting parsing`)
    
    return [setting, value]
}

// Set a new channel (only channel for now)
export async function hbset(object : ChatInputCommandInteraction | Message) {
    // Gather arguments
    let [setting, value] = ["", ""]
    try {
        [setting, value] = parseSettingCommand(object)
    } catch (error) {
        logErr(object, `Error occured while parsing : ${error}`)
        await tryReply(object, lang.set.channel.errors.WRONG_USAGE)
        return
    }

    // Apply setting 
    let channel : Channel   | null 
    let guild   : Guild     | null 

    try {
        guild = object.guild
    } catch(error) {
        logErr(object, `Error fetching guild : ${error}`)
        await tryReply(object, lang.set.channel.errors.CHANNEL_NOT_FOUND)
        return
    }

    if (guild) {
        // Try to fetch channel
        try {
            channel = await guild.channels.fetch(value)
        } catch (error) {
            logErr(object, `Error fetching channel : ${error}`)
            await tryReply(object, lang.set.channel.errors.CHANNEL_NOT_FOUND)
            return 
        }
    
        if (channel) {
            // Channel exist
            if (channel.type == ChannelType.GuildText) {
                // Channel is text based
                // Apply settings
                database.addSettings(<string>object.guildId,channel.id)
                try {
                    await tryReply(object, lang.set.channel.success.REPLY)
                    await channel.send(lang.set.channel.success.CHANNEL_MESSAGE)
                } catch (error) {
                    logErr(object, `Error occured while replying : ${error}`)
                }
            } else {
                // Channel is not text based
                logErr(object, `Channel is not text-based!`)
                await tryReply(object, lang.set.channel.errors.WRONG_CHANNEL_TYPE)
            }
        } else {
            // Channel not found
            logErr(object, `Channel not found!`)
            await tryReply(object, lang.set.channel.errors.CHANNEL_NOT_FOUND)
        }
    } else {
        // Channel not found
        logErr(object, `Guild not found!`)
        await tryReply(object, lang.errors.GENERIC)
    }
}