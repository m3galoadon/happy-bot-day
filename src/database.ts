// used to handle databases
import * as fs from "fs"
import { logErr } from "./glog"

// Guild setting : 
// guildId --> Id of the guild the settings apply to
// birthdayChannelId --> Id of the channel to tell happy birthday
class GuildSettings {
    guildId: string
    birthdayChannelId: string

    // GuildSetting constructor
    constructor(guildID : string, birthdayChannelId : string) {
        this.birthdayChannelId = birthdayChannelId
        this.guildId = guildID
    }

    // Create new GuildSettings from string (from db file)
    public static fromString(input : string) : GuildSettings {
        // Check if data is correctly formed
        if (input.split(":").length != 2) {
            throw new Error("Error loading config file : Malformed guildSetting string in constructor!")
        }

        // Get values and set them
        let s : string[] = input.split(":")
        return new GuildSettings(s[0], s[1])
    }
}

// Date type aliases
export type Day = number
export type Month = number 
export type Date = [Day, Month]

// Birthday data
// clientId --> Id of the member
// date --> Date object (Day, Month)
class Birthday {
    public clientId: string
    public date: Date

    // New birthday constructor from database
    constructor(id : string, date : Date) {
        this.clientId = id 
        this.date = date
    }

    // Create a new birthday from string
    public static fromString(input : string) : Birthday {    
        // Check if data is correctly formed
        if (input.split(":").length != 2) {
            throw new Error(`Malformed birthday string in constructor : ${input}`)
        }
        
        // Init vars
        let date : Date
        let clientId : string

        // Get values and set them
        clientId = input.split(":")[0]
        let dateBase = input.split(":")[1].split("/")
        if (dateBase.length != 2) {
            throw new Error("Malformed date!")
        }

        // String to date
        try {
            let day = +dateBase[0]
            let month = +dateBase[1]
            date = [ day, month ]
        } catch(error) {
            throw new Error(`Error creating 'Date' from string : ${error}`)
        }

        return new Birthday(clientId, date)
    }
}

// Full database for the bot
// guildSettings    --> List of guildSettings
// birthdays        --> List of birthdays
export class Database {
    public guildSettings: GuildSettings[]
    public birthdays: Birthday[]

    // Init database
    constructor() {
        this.birthdays = []
        this.guildSettings = []
        this.getFromFile()
    }

    // Update database from file
    public getFromFile() {
        // Try to load config
        let guildSettingsBuffer : Buffer | string
        try {
            guildSettingsBuffer = fs.readFileSync('data/hbdcfg')
        } catch(error) {
            logErr(undefined, `Error loading database : ${error}`)
            guildSettingsBuffer = Buffer.from("")
        } 
        
        // Try to load users
        let birthdaysBuffer : Buffer
        try {
            birthdaysBuffer = fs.readFileSync("data/hbddb")
        } catch(error) {
            logErr(undefined, `Error loading birthdays : ${error}`)
            birthdaysBuffer = Buffer.from("")
        }

        // Load GuildSettings
        let guildSettingsArray : GuildSettings[] = []
        for (const line of guildSettingsBuffer.toString().split("\n")) {
            if (line != "") guildSettingsArray.push(GuildSettings.fromString(line))
        }

        // Load Birthdays
        let birthdaysArray : Birthday[] = []
        for (const line of birthdaysBuffer.toString().split("\n")) {
            if (line != "") birthdaysArray.push(Birthday.fromString(line))
        }

        this.birthdays = birthdaysArray
        this.guildSettings = guildSettingsArray
    }

    // Add entry to birthday database
    public addEntry(userId : string, date: Date) {
        // Check
        let done = false 

        // Check if birthday already exist and modify or add it
        for (let birthday of this.birthdays) {
            if (userId == birthday.clientId) {
                // Replace birthday and overwrite file
                birthday.date = date
                done = true 
                break 
            }
        }

        // Push to actual db
        if (!done) this.birthdays.push(new Birthday(userId, date))

        // try to rewrite birthday file
        try {
            this.writeBirthdays()
        } catch(error) {
            logErr(undefined, `An error occured while writting to Birthdays File : ${error}`)
        }
    }

    // Adds GuildSettings entry (or modify one)
    public addSettings(guildId: string, channelId : string) {
        // Check
        let done = false 

        // Check if birthday already exist and modify or add it
        for (let guildSettings of this.guildSettings) {
            if (guildId == guildSettings.guildId) {
                // Replace birthday and overwrite file
                guildSettings.birthdayChannelId = channelId
                done = true 
                break 
            }
        }

        // Push to actual db
        if (!done) this.guildSettings.push(new GuildSettings(guildId, channelId))

        // try to rewrite birthday file
        try {
            this.writeSettings()
        } catch(error) {
            logErr(undefined, `An error occured while writting to GuildSettings File : ${error}`)
        }
    }

    // Write birthdays to file
    public writeBirthdays() {
        let buffer : string = ""
        for (const birthday of this.birthdays) {
            buffer += `${birthday.clientId}:${birthday.date[0]}/${birthday.date[1]}\n`
        }

        // write file
        try {
            fs.writeFileSync("data/hbddb", buffer)
        } catch (error) {
            throw error
        }
    }

    // Write settings to file
    public writeSettings() {
        let buffer : string = ""
        for (const settings of this.guildSettings) {
            buffer += `${settings.guildId}:${settings.birthdayChannelId}\n`
        }

        // write file
        try {
            fs.writeFileSync("data/hbdcfg", buffer)
        } catch (error) {
            throw error
        }
    }
}