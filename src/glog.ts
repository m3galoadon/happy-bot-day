// Grepable logs 
// [dd/mm/yy]:[hh:mm:ss]:{username}:{id}:{message}:{additionnal_info}

import { Interaction, Message } from "discord.js";
import * as fs from "fs"

// File where to stock logs
const logFile = "data/logs"

// Basic log
export function log(object? : Interaction | Message, additionnalInfo? : string ) {
    // Quit if nothing has been logged
    if (!object && !additionnalInfo) throw new Error("Wrong usage : Please send information to log!")

    // Create log
    let msg : string 
    try { msg = buildLog("i", object, additionnalInfo) } 
    catch(error) { 
        console.error(`Error in buildLog : ${error}`) 
        return
    }

    // Append message to logs
    try { fs.appendFileSync(logFile, msg + '\n') }
    catch (error) { console.error(`Error appending log to file : ${error}`) }

    // Log to console
    console.log(msg)
}

// Error log
export function logErr(object? : Interaction | Message, additionnalInfo? : string ) {
    // Quit if nothing has been logged
    if (!object && !additionnalInfo) throw new Error("Wrong usage : Please send information to log!")

    // Create error log
    let msg : string 
    try { msg = buildLog("!", object, additionnalInfo) } 
    catch(error) { 
        console.error(`Error in buildLog : ${error}`) 
        return
    }

    // Append message to logs
    try { fs.appendFileSync(logFile, msg + '\n') } 
    catch (error) { console.error(`Error appending log to file : ${error}`) }

    // Log to console
    console.error(msg)
}

// Build grepable log
function buildLog(logo : string, object? : Message | Interaction, additionnalInfo? : string) : string {
    try {

        // Quit if nothing has been logged
        if (!object && !additionnalInfo) throw new Error("Wrong usage : Please send information to log!")
    
        // Get date
        let date = new Date()
    
        // Init log
        // Date
        let msg = `(${logo}) [${('00'+(date.getDate())).slice(-2)}/${('00'+(date.getMonth() +1)).slice(-2)}/${date.getFullYear()}]`
        // Time
        msg += `:[${('00'+date.getHours()).slice(-2)}:${('00'+date.getMinutes()).slice(-2)}:${('00'+date.getSeconds()).slice(-2)}]`
    
        if(object) {
            // Add user
            if (object instanceof Message) {
                let obj = <Message> object 
                msg += ` | USER: { ${obj.author.username} (${obj.author.id}) }`
            } else {
                let obj = <Interaction> object 
                msg += ` | USER: { ${obj.user.username} (${obj.user.id}) }`
            }
        
            // Add channel
            msg += ` | CHANNEL: { ${object.channelId} }`
        
            // Add guild
            msg += ` | GUILD: { ${object.guild} (${object.guildId}) }`
        
            // Add message
            if (object instanceof Message) msg += ` | MESSAGE: { ${(<Message>object).content} }` 
            else {
                let obj = <Interaction> object 
                if (obj.isCommand()) {
                    msg += ` | MESSAGE: { /${obj.commandName}`
                    for (const argument of obj.options.data) {
                        msg += ` ${argument}`
                    }
                    msg += " }"
                }
            }
        }
    
        // Add additionnal informations
        if (additionnalInfo) msg += ` | INFOS: { ${additionnalInfo} }`
    
        // Return with new line
        return msg
    } catch (error) {
        return `Fatal error in gLog : ${error}`
    }
}